<?php
namespace Randevmodule\Eclated\Block;
class Display extends \Magento\Framework\View\Element\Template
{
    public function __construct(\Magento\Framework\View\Element\Template\Context $context)
    {
        parent::__construct($context);
    }

    public function getAll(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('assoc_product_eclated');
        $sql = "SELECT img FROM " . $tableName." GROUP BY img";
        $result = $connection->fetchAll($sql);
        if (isset($result)){
            return $result;
        }else{
            return null;
        }

    }
}