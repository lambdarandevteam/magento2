<?php


namespace Randevmodule\Importeclatedauto\Block;
use Magento\Framework\App\Bootstrap;
class Blockeclated extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function index()
    {
        //Your block code
        return __('Hello Developer! This how to get the storename: %1 and this is the way to build a url: %2', $this->_storeManager->getStore()->getName(), $this->getUrl('contacts'));
    }
    public function importall(){
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $state = $objectManager->get('Magento\Framework\App\State');
        $state->setAreaCode('frontend');
        $simple_product = $objectManager->create('\Magento\Catalog\Model\Product');
        $simple_product->setSku('tops123');
        $simple_product->setName('tops');
        $simple_product->setAttributeSetId(4);;
        $simple_product->setCategories('Default Category/Women');
        $simple_product->setStatus(1);
        $simple_product->setTypeId('simple');
        $simple_product->setPrice(10);
        $simple_product->setWebsiteIds(array(1));
        $simple_product->setCategoryIds(array(31));
        $simple_product->setUrlKey ('tops343225');
        $simple_product->setColor('Red');
        $simple_product->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
                'is_in_stock' => 1, //Stock Availability
                'qty' => 100 //qty
            )
        );


        $simple_product->save();
        $simple_product_id = $simple_product->getId();
        echo "simple product id: ".$simple_product_id."\n";
    }
}
