<?php


namespace Randevmodule\Importeclatedauto\Block\Adminhtml\Index;
use Magento\Framework\Module\Dir;
class Index extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    protected $categoryLinkManagement;
    private $productRepository;
    private $xlsxparser;
    protected $data = [];

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagementInterface,
        \Magento\Framework\Module\Dir\Reader $reader,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->categoryLinkManagement = $categoryLinkManagementInterface;
        $this->moduleReader = $reader;
        $this->productRepository = $productRepository;
    }
    public function getModuledir()
    {
        $moduleDirectory = $this->moduleReader->getModuleDir(Dir::MODULE_VIEW_DIR, 'Randevmodule_Importeclatedauto');
        return $moduleDirectory;
    }
    public function index(){
        echo "hello!!!!";
    }
    public function importall(){
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('structure_catalogue');
        $sql = "SELECT * FROM " . $tableName;
        $result = $connection->fetchAll($sql);
        $row = 1;
//        echo '<pre>'; print_r($result); echo '</pre>';
            foreach ($result as $data) {
                $row++;
                    $testexist_product_repair = $this->check_produit_reparer(preg_replace('/[^a-zA-Z0-9_.]/', '_', $data['Nom_Produit']));
                    if($testexist_product_repair == false ){
                        $id_product_reparer = $this->insert_produit_reparer($data);
                        $existed = 0;
                        $this->insert_brand_produit_reparer($data['Marque'],$id_product_reparer);
                    }else{
                        $existed = 1;
                        $id_product_reparer = $testexist_product_repair;
                    }
                    $testexist_product_detache = $this->check_produit_reparer($data['Reference_Piece']);
                    if($testexist_product_detache == false ){
                          $id_product_detache = $this->insert_produit_detache($data);
                          $this->insert_brand_produit_detache($data["Marque"],$id_product_detache);
                    }else{
                        $id_product_detache = $testexist_product_detache;
                    }
                    if (isset($id_product_detache) AND isset($id_product_reparer)){

                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
                        $mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath().'images/Archive/'.str_replace("Ã©","é",$data['Modele'])."/vue/";
                        $files = scandir(str_replace("Ã©","é",$mediaPath));
                        if (isset($files) AND $files != null AND $files !=false){
                            foreach ($files as $allimg){
                                       $img_eclated = $allimg;
                            }
                        }
                        if (!isset($img_eclated)){
                            $img_eclated ="";
                        }else{
                            if ($existed ==0){
                                var_dump($mediaPath.$img_eclated);
                            $this->parse_to_ocr_by_link($img_eclated,$mediaPath.$img_eclated);
                            }
                        }
                        $this->assoc_product_repair_and_product_detached($id_product_reparer,$id_product_detache,$img_eclated,$data['Repere_vue_eclatee']);
                    }else{
                        echo "assoc failed";
                    }
            }
        die("sdsd");
    }
    public function insert_produit_reparer($data){

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
        $mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();

        $catId = 66;  //Parent Category ID
        $catId_univ = 2;  //Parent Category ID
        $catId_modele = 83;  //Parent Category ID

        $subCategory = $objectManager->create('Magento\Catalog\Model\Category')->load($catId);
        $subCats = $subCategory->getChildrenCategories()->addAttributeToSort('name', 'asc');

        $subCategory_univers = $objectManager->create('Magento\Catalog\Model\Category')->load($catId_univ);
        $subCats_univers = $subCategory_univers->getChildrenCategories()->addAttributeToSort('name', 'asc');

        $subCategory_modele = $objectManager->create('Magento\Catalog\Model\Category')->load($catId_modele);
        $subCats_modele = $subCategory_modele->getChildrenCategories()->addAttributeToSort('name', 'asc');

        $is_categ_type_exists =0;
        $is_categ_univers_exists =0;
        $is_categ_univers_exists =0;
        $categids = [];
        foreach ($subCats as $sub){
//            echo $sub->getName();
            if (preg_match("/".$data['Type']."/",$sub->getName())){
                $categ = $sub->getId();
                array_push($categids,$categ);
                $is_categ_type_exists =1;
            }
        }
        foreach ($subCats_univers as $sub){
//            echo $sub->getName();
            if (preg_match("/".$data['Univers']."/",$sub->getName())){
                $categ = $sub->getId();
                if (isset($categ) AND $categ != "" AND $categ != null){
                    $catId_univ_child = $categ;  //Parent Category ID
                    $subCategory_univers_child = $objectManager->create('Magento\Catalog\Model\Category')->load($catId_univ_child);
                    $subCats_univers_child = $subCategory_univers_child->getChildrenCategories()->addAttributeToSort('name', 'asc');
                    foreach ($subCats_univers_child as $categ_child_item){
                        $categ_child = $categ_child_item->getId();
                        array_push($categids,$categ_child);
                    }
                }

                $is_categ_univers_exists =1;
            }
        }
        foreach ($subCats_modele as $sub){
//            echo $sub->getName();
            if (preg_match("/".$data['Modele']."/",$sub->getName())){
                $categ = $sub->getId();
                array_push($categids,$categ);
                $is_categ_univers_exists =1;
            }
        }

        $simple_product = $objectManager->create('\Magento\Catalog\Model\Product');
        $simple_product->setSku(preg_replace('/[^a-zA-Z0-9_.]/', '_', $data['Nom_Produit'])); //1
        $simple_product->setName($data['Nom_Produit']); //2
        $simple_product->setAttributeSetId(4);
        $simple_product->setDescription($data["Description"]);
        $simple_product->setStatus(1); //2
        $simple_product->setTypeId('ProductRepair'); //3
        $simple_product->setPrice(10); //4
        $simple_product->setWebsiteIds(array(1));

        $simple_product->setCategoryIds($categids); //5
        $simple_product->setUrlKey (preg_replace('/[^a-zA-Z0-9_.]/', '_', $data['Nom_Produit']).rand(0, 100));
        $simple_product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
        $simple_product->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                'max_sale_qty' => 100, //Maximum Qty Allowed in Shopping Cart
                'is_in_stock' => 1, //Stock Availability
                'qty' => 10 //qty  //6
            )
        );
        $simple_product->save();
        $simple_product_id = $simple_product->getId();
        /*Add Images To The Product*/

        $galleryReadHandler = $objectManager->create('Magento\Catalog\Model\Product\Gallery\ReadHandler');
        $imageProcessor = $objectManager->create('Magento\Catalog\Model\Product\Gallery\Processor');
        $productGallery = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Gallery');
        if ($simple_product) {
            $galleryReadHandler->execute($simple_product);

            // Unset existing images
            $images = $simple_product->getMediaGalleryImages();
            foreach($images as $child) {
                $productGallery->deleteGallery($child->getValueId());
                $imageProcessor->removeImage($simple_product, $child->getFile());
            }
            /**
             * Add image. Image directory must be in ROOT/pub/media for addImageToMediaGallery() method to work
             */

            $imagePath_f = $mediaPath."images/Archive/".str_replace("Ã©","é",$data['Modele'])."/images/"; // path of the image.
            if (scandir($imagePath_f)){
            $files = scandir($imagePath_f);
            foreach ($files as $allimg){
//                if (isset(explode(".",$allimg)[0])){
//                    $replacements = array('á'=>'a','ã'=>'a',"é"=>"e","è"=>"e","ç"=>"c");
//                    $modele = strtr($data['Modele'],$replacements);
//
//                    if (preg_match("/".$modele."/i",$allimg)){
                        $imagePath = $allimg;
//                    }
//                }
            }
            }
            if (!isset($imagePath) || $imagePath == false || $imagePath == null ){
                $imagePath ="";
            }else{
                $imagePath=$mediaPath."images/Archive/".str_replace("Ã©","é",$data['Modele'])."/images/".$imagePath;
            }
            if ($imagePath !=""){
                var_dump($imagePath);
            $simple_product->addImageToMediaGallery($imagePath, array('image', 'small_image', 'thumbnail'), false, false);
            $simple_product->save();
            echo "Added media image for {$simple_product_id}" . "</br>";
            }
        }
        return $simple_product_id;
    }
    public function check_produit_reparer($data){
        try {
        $response = $this->productRepository->get($data);
            return  $response->getId();
        }catch (\Magento\Framework\Exception\NoSuchEntityException $e){
               return false;
        }
    }
    public function insert_produit_detache($data){
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
        $mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
        $simple_product = $objectManager->create('\Magento\Catalog\Model\Product');
        $simple_product->setSku($data['Reference_Piece']); //1
        $simple_product->setName($data['Nom_Piece']); //2
        $simple_product->setAttributeSetId(4);
        $simple_product->setStatus(1); //2
        $simple_product->setDescription($data['Description_Piece']);
        $simple_product->setTypeId('simple'); //3
        $simple_product->setPrice((int)$data['Prix']); //4
        $simple_product->setWebsiteIds(array(1));


        $catId = 66;  //Parent Category ID
        $catId_univ = 2;  //Parent Category ID
        $catId_modele = 83;  //Parent Category ID

        $subCategory = $objectManager->create('Magento\Catalog\Model\Category')->load($catId);
        $subCats = $subCategory->getChildrenCategories()->addAttributeToSort('name', 'asc');

        $subCategory_univers = $objectManager->create('Magento\Catalog\Model\Category')->load($catId_univ);
        $subCats_univers = $subCategory_univers->getChildrenCategories()->addAttributeToSort('name', 'asc');

        $subCategory_modele = $objectManager->create('Magento\Catalog\Model\Category')->load($catId_modele);
        $subCats_modele = $subCategory_modele->getChildrenCategories()->addAttributeToSort('name', 'asc');

        $categids = [];

        foreach ($subCats as $sub){
//            echo $sub->getName();
            if (preg_match("/".$data['Type']."/",$sub->getName())){
                $categ = $sub->getId();
                array_push($categids,$categ);
                $is_categ_type_exists =1;
            }
        }
        foreach ($subCats_univers as $sub){
//            echo $sub->getName();
            if (preg_match("/".$data['Univers']."/",$sub->getName())){
                $categ = $sub->getId();
                array_push($categids,$categ);
                $is_categ_univers_exists =1;
            }
        }
        foreach ($subCats_modele as $sub){
//            echo $sub->getName();
            if (preg_match("/".$data['Modele']."/",$sub->getName())){
                $categ = $sub->getId();
                array_push($categids,$categ);
                $is_categ_univers_exists =1;
            }
        }
        if (isset($data['nombre_requis']) AND $data['nombre_requis'] != null AND $data['nombre_requis'] !="" ){
            $qtty_min = (int)$data['nombre_requis'];
        }else{
            $qtty_min = 1;
        }

        $simple_product->setCategoryIds($categids); //5
        $simple_product->setUrlKey (preg_replace('/[^a-zA-Z0-9_.]/', '_', $data['Nom_Piece']).rand(0, 100));
        $simple_product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);

        $simple_product->setStockData(

            array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => $qtty_min, //Minimum Qty Allowed in Shopping Cart
                'max_sale_qty' => 100, //Maximum Qty Allowed in Shopping Cart
                'is_in_stock' => 1, //Stock Availability
                'qty' => (int)$data["Stock"] ?? 0 //qty  //6
            )

        );

        $simple_product->save();
        $simple_product_id = $simple_product->getId();
        /*Add Images To The Product*/

        $galleryReadHandler = $objectManager->create('Magento\Catalog\Model\Product\Gallery\ReadHandler');
        $imageProcessor = $objectManager->create('Magento\Catalog\Model\Product\Gallery\Processor');
        $productGallery = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Gallery');
            if ($simple_product) {
                $galleryReadHandler->execute($simple_product);

                // Unset existing images
                $images = $simple_product->getMediaGalleryImages();
                foreach($images as $child) {
                    $productGallery->deleteGallery($child->getValueId());
                    $imageProcessor->removeImage($simple_product, $child->getFile());
                }
                /**
                 * Add image. Image directory must be in ROOT/pub/media for addImageToMediaGallery() method to work
                 */

                $imagePath_f = $mediaPath."images/Archive/".$data['Modele']."/images/"; // path of the image.
                $files = scandir(str_replace("Ã©","é",$imagePath_f));
                if (isset($files) AND $files != null AND $files != false ){
                foreach ($files as $allimg){
                            $imagePath_fiole = $allimg;
                }
                }
                if (!isset($imagePath_fiole) || $imagePath_fiole == false || $imagePath_fiole == null ) {

                }else{

                $imagePath = $mediaPath."images/Archive/".str_replace("Ã©","é",$data['Modele'])."/images/".$imagePath_fiole;
                var_dump($imagePath);
                                $simple_product->addImageToMediaGallery($imagePath, array('image', 'small_image', 'thumbnail'), false, false);
                $simple_product->save();
                echo "Added media image for {$simple_product_id}" . "</br>";

                }
            }
        return $simple_product_id;
    }

    public function insert_brand_produit_reparer($nom_marque,$id_produit){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sql_check = "SELECT * FROM `ves_brand` WHERE `name` LIKE '%$nom_marque%' ";
        $result_check = $connection->query($sql_check);
        if ($result_check !=[] AND $result_check != null AND $result_check != "" ){
            foreach ($result_check as $res){
                $idmarque = $res['brand_id'];
            }
        }else{
            $idmarque = "";
        }
        if ($idmarque !=""){

        $sql = "INSERT INTO `ves_brand_product`(`brand_id`, `product_id`, `position`) VALUES ($idmarque,$id_produit,0)";
        $result = $connection->query($sql);
        return $result;
        }
    }

    public function insert_brand_produit_detache($nom_marque,$id_produit){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sql_check = "SELECT * FROM `ves_brand` WHERE `name` LIKE '%$nom_marque%' ";
        $result_check = $connection->query($sql_check);
        if ($result_check !=[] AND $result_check != null AND $result_check != "" ){
            foreach ($result_check as $res){
                $idmarque = $res['brand_id'];
            }
        }else{
            $idmarque = "";
        }
        if ($idmarque !=""){

            $sql = "INSERT INTO `ves_brand_product`(`brand_id`, `product_id`, `position`) VALUES ($idmarque,$id_produit,0)";
            $result = $connection->query($sql);
            return $result;
        }
    }

    public function assoc_product_repair_and_product_detached($id_reparer,$id_detache,$img_vue_eclate,$nb_eclated){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = "INSERT INTO `assoc_product_eclated`(`url_product`, `id_number`, `img`,`id_reparer`) VALUES ($id_detache,'$nb_eclated','$img_vue_eclate','$id_reparer')";
        $result = $connection->query($sql);
        return $result;
    }

    public function parse_to_ocr_by_link($file_nam,$link){
            $file_image_path = str_replace("/var/www/html/",'http://'.$_SERVER['SERVER_NAME'].'/',$link);
            $this->parse_all($file_nam,$file_image_path);
    }

    function file_get_contents_curl($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
    function parse_all($filename,$fullpath){
    $this->uploadToApi($filename,$fullpath);
    }
   public function uploadToApi($filename,$target_file){
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
       $connection = $resource->getConnection();
        require __DIR__ . '/vendor/autoload.php';
        $fileData = fopen(str_replace(" ","%20",$target_file), 'r');
        var_dump($fileData);
        $client = new \GuzzleHttp\Client();
       try {

            $r = $client->request('POST', 'https://apipro2.ocr.space/parse/image',[
                'headers' => ['apiKey' => 'PKMXB8221888A'],
                'multipart' => [
                    [
                        'name' => 'file',
                        'contents' => $fileData,
                    ],
                    [
                        'name' => 'isOverlayRequired',
                        'contents' => "true",
                    ]
                ]
            ], ['file' => $fileData]);
            $response =  json_decode($r->getBody(),true);
            $word = array_merge(range('a', 'z'), range('A', 'Z'));
            shuffle($word);
                $result_to_database = "";
                if (isset($response['ParsedResults'])){
                    foreach($response['ParsedResults'] as $pareValue) {
                        $result_to_database .= $pareValue['ParsedText']." ";
                        foreach($pareValue as $res){
                            if (isset($res['Lines'])){
                                foreach($res['Lines'] as $to_save){
                                    $MaxHeight = $to_save['MaxHeight'];
                                    $MinTop = $to_save['MinTop'];
                                    $LineText = $to_save['LineText'];
                                    $WordText = preg_replace('/-/', '', $to_save['Words'][0]['WordText']);
                                    $Left = $to_save['Words'][0]['Left'];
                                    $Top= $to_save['Words'][0]['Top'];
                                    $Height = $to_save['Words'][0]['Height'];
                                    $Width = $to_save['Words'][0]['Width'];
                                    $datetime = date('Y-m-d H:i:s');
                                    $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;
                                    if($WordText != "" && $WordText !=" "){
                                        $sql = "INSERT INTO `ocr_cabatho_data` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                                        if($result = $connection->query($sql)){
                                        } else{
                                            echo "ERROR: Could not able to execute 1";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $this->uploadToApimoteur1_active($target_file,$filename);
                }
        } catch(Exception $err) {
            header('HTTP/1.0 403 Forbidden');
            echo $err->getMessage();
        }
    }
    public function uploadToApimoteur1_active($target_file,$filename){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        require __DIR__ . '/vendor/autoload.php';
        $fileData = fopen(str_replace(" ","%20",$target_file), 'r');
        $client = new \GuzzleHttp\Client();
        try {
            $r = $client->request('POST', 'https://apipro2.ocr.space/parse/image',[
                'headers' => ['apiKey' => 'PKMXB8221888A'],
                'multipart' => [
                    [
                        'name' => 'file',
                        'contents' => $fileData,
                    ],
                    [
                        'name' => 'isOverlayRequired',
                        'contents' => "true",
                    ],
                    [
                        'name' => 'scale',
                        'contents' => "true",
                    ]
                ]
            ], ['file' => $fileData]);
            $response =  json_decode($r->getBody(),true);


            $word = array_merge(range('a', 'z'), range('A', 'Z'));
            shuffle($word);
                $result_to_database = "";
                if ($response['ParsedResults']){
                foreach($response['ParsedResults'] as $pareValue) {
                    $result_to_database .= $pareValue['ParsedText']." ";
                    foreach($pareValue as $res){
                        if (isset($res['Lines'])){
                        foreach($res['Lines'] as $to_save){

                            $MaxHeight = $to_save['MaxHeight'];
                            $MinTop = $to_save['MinTop'];
                            $LineText = $to_save['LineText'];
                            $WordText = preg_replace('/-/', '', $to_save['Words'][0]['WordText']);
                            $Left = $to_save['Words'][0]['Left'];
                            $Top= $to_save['Words'][0]['Top'];
                            $Height = $to_save['Words'][0]['Height'];
                            $Width = $to_save['Words'][0]['Width'];
                            $datetime = date('Y-m-d H:i:s');

                            $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;

                            // foreach()
                            if($WordText != "" && $WordText !=" "){
                                $sql = "INSERT INTO `ocr_cabatho_data_active` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                                if($connection->query($sql)){

                                } else{
                                    echo "ERROR: Could not able to execute 2";
                                }
                            }
                        }
                    }
                    }
                }
                    if (!preg_match("/.gif/",$filename)){
                        $this->uploadToApiMoteur2($target_file,$filename);
                    }else{
                        $this->synchronizeall($filename);
                    }
                }


        } catch(Exception $err) {
            header('HTTP/1.0 403 Forbidden');
            echo $err->getMessage();
        }
    }

    public function uploadToApiMoteur2($target_file,$filename){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        require __DIR__ . '/vendor/autoload.php';
        $fileData = fopen(str_replace(" ","%20",$target_file), 'r');
        $client = new \GuzzleHttp\Client();
        try {
            $r = $client->request('POST', 'https://apipro2.ocr.space/parse/image',[
                'headers' => ['apiKey' => 'PKMXB8221888A'],
                'multipart' => [
                    [
                        'name' => 'file',
                        'contents' => $fileData,
                    ],
                    [
                        'name' => 'isOverlayRequired',
                        'contents' => "true",
                    ],
                    [
                        'name' => 'OCREngine',
                        'contents' => "2",
                    ]
                ]
            ], ['file' => $fileData]);
            $response =  json_decode($r->getBody(),true);
            $word = array_merge(range('a', 'z'), range('A', 'Z'));
            shuffle($word);
                $result_to_database = "";
                if (isset($response['ParsedResults'])){
                foreach($response['ParsedResults'] as $pareValue) {
                    $result_to_database .= $pareValue['ParsedText']." ";
                    foreach($pareValue as $res){

                        if (isset($res['Lines'])){
                        foreach($res['Lines'] as $to_save){


                            $MaxHeight = $to_save['MaxHeight'];
                            $MinTop = $to_save['MinTop'];
                            $LineText = $to_save['LineText'];
                            $WordText =preg_replace('/-/', '',$to_save['Words'][0]['WordText']);
                            $Left = $to_save['Words'][0]['Left'];
                            $Top= $to_save['Words'][0]['Top'];
                            $Height = $to_save['Words'][0]['Height'];
                            $Width = $to_save['Words'][0]['Width'];
                            $datetime = date('Y-m-d H:i:s');

                            $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;

                            // foreach()
                            if($WordText !="" && $WordText != " "){
                                $sql = "INSERT INTO `ocr_cabatho_data_engine2` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                                if($connection->query($sql)){

                                } else{
                                    echo "ERROR: Could not able to execute 3";
                                }
                            }
                        }
                    }
                    }
                }
                    $this->uploadToApimoteur2_active($target_file,$filename);
                }


        } catch(Exception $err) {
            header('HTTP/1.0 403 Forbidden');
            echo $err->getMessage();
        }
    }
    public function uploadToApimoteur2_active($target_file,$filename){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        require __DIR__ . '/vendor/autoload.php';

        $fileData = fopen(str_replace(" ","%20",$target_file), 'r');

        $client = new \GuzzleHttp\Client();
        try {
            $r = $client->request('POST', 'https://apipro2.ocr.space/parse/image',[
                'headers' => ['apiKey' => 'PKMXB8221888A'],
                'multipart' => [
                    [
                        'name' => 'file',
                        'contents' => $fileData,
                    ],
                    [
                        'name' => 'isOverlayRequired',
                        'contents' => "true",
                    ],
                    [
                        'name' => 'OCREngine',
                        'contents' => "2",
                    ],
                    [
                        'name' => 'scale',
                        'contents' => "true",
                    ]

                ]
            ], ['file' => $fileData]);
            $response =  json_decode($r->getBody(),true);

            $word = array_merge(range('a', 'z'), range('A', 'Z'));
            shuffle($word);

                $result_to_database = "";
                if (isset($response['ParsedResults'])){
                foreach($response['ParsedResults'] as $pareValue) {
                    $result_to_database .= $pareValue['ParsedText']." ";
                    foreach($pareValue as $res){
                        if (isset($res['Lines'])){
                        foreach($res['Lines'] as $to_save){

                            $MaxHeight = $to_save['MaxHeight'];
                            $MinTop = $to_save['MinTop'];
                            $LineText = $to_save['LineText'];
                            $WordText = preg_replace('/-/', '', $to_save['Words'][0]['WordText']);
                            $Left = $to_save['Words'][0]['Left'];
                            $Top= $to_save['Words'][0]['Top'];
                            $Height = $to_save['Words'][0]['Height'];
                            $Width = $to_save['Words'][0]['Width'];
                            $datetime = date('Y-m-d H:i:s');

                            $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;

                            // foreach()
                            if($WordText != "" && $WordText !=" "){

                                $sql = "INSERT INTO `ocr_cabatho_data_engine_active` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                                if($connection->query($sql)){
                                } else{
                                    echo "ERROR: Could not able to execute 4";
                                }
                            }
                        }
                        }
                    }
                }
                    $this->synchronizeall($filename);
                }



        } catch(Exception $err) {
            header('HTTP/1.0 403 Forbidden');
            echo $err->getMessage();
        }
    }
   public function synchronizeall($filename){
        $this->insert1($filename);
    }
    function insert1($filename)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $file_name = $filename;
        if (isset($file_name) && $file_name != null){
            $query2 = "SELECT * FROM ocr_cabatho_data WHERE file_name = '".$file_name."'";

            foreach($connection->fetchAll($query2) as $row2) {
                $MaxHeight = $row2['MaxHeight'];
                $MinTop = $row2['MinTop'];
                $LineText = $row2['LineText'];
                $WordText =preg_replace('/-/', '',$row2['WordText']);
                $Left = $row2['pos_Left'];
                $Top= $row2['pos_Top'];
                $Height = $row2['pos_Height'];
                $Width = $row2['Width'];
                $datetime = date('Y-m-d H:i:s');
                $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;
                $result_to_database = $row2['data'];
                $filename =$row2['file_name'];
                $target_file =$row2['id_save'];
                if($WordText !="" && $WordText !=" "){
                    $query_insert = "INSERT INTO `ocr_cabatho_data_sync_ok` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                    if($connection->query($query_insert)){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }

            }
            $this->insert2($filename);

            if(!preg_match("/.gif/",$filename)){
                $this->insert3($filename);
                $this->insert4($filename);
            }
        }
    }
    function insert2($file_name)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();


        $array_check2 = [];
        $query3 = "SELECT * FROM ocr_cabatho_data_sync_ok WHERE file_name = '".$file_name."'";

        foreach($connection->fetchAll($query3) as $row3) {
            array_push($array_check2,$row3['WordText']);
        }

        $query5 = "SELECT * FROM ocr_cabatho_data_active WHERE file_name = '".$file_name."'";

        foreach($connection->fetchAll($query5) as $row4) {
            if (in_array($row4['WordText'],$array_check2)){
                echo 'c';
            }else{
                $MaxHeight2 = $row4['MaxHeight'];
                $MinTop2 = $row4['MinTop'];
                $LineText2 = $row4['LineText'];
                $WordText2 =preg_replace('/-/', '',$row4['WordText']);
                $Left2 = $row4['pos_Left'];
                $Top2= $row4['pos_Top'];
                $Height2 = $row4['pos_Height'];
                $Width2 = $row4['Width'];
                $datetime2 = date('Y-m-d H:i:s');
                $to_save_field2 = ",'".$LineText2."','".$WordText2."','".$Left2."','".$Top2."','".$Height2."','".$Width2;
                $result_to_database2 = $row4['data'];
                $filename2 =$row4['file_name'];
                $target_file2 =$row4['id_save'];
                if($WordText2 !="" && $WordText2 !=" "){
                    $query_insert2 = "INSERT INTO `ocr_cabatho_data_sync_ok` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file2."','".str_replace("'","",$result_to_database2)."','".$filename2."','".$datetime2."'".$to_save_field2."','".$MaxHeight2."','".$MinTop2."')";
                    if($connection->query($query_insert2)){
                        echo 'ok moteur 1 active';
                    }else{
                        echo 'ereur moteur 1 active';
                    }
                }

            }
        }
    }
    function insert3($filename)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();


        $file_name = $filename;

        $array_check3 = [];
        $query3 = "SELECT * FROM ocr_cabatho_data_sync_ok WHERE file_name = '".$file_name."'";
        foreach($connection->fetchAll($query3) as $row3) {
            array_push($array_check3,$row3['WordText']);
        }
        $query5 = "SELECT * FROM ocr_cabatho_data_engine2 WHERE file_name = '".$file_name."'";
        foreach($connection->fetchAll($query5) as $row4) {
            if (in_array($row4['WordText'],$array_check3)){
                echo 'c';
            }else{
                $MaxHeight2 = $row4['MaxHeight'];
                $MinTop2 = $row4['MinTop'];
                $LineText2 = $row4['LineText'];
                $WordText2 =preg_replace('/-/', '',$row4['WordText']);
                $Left2 = $row4['pos_Left'];
                $Top2= $row4['pos_Top'];
                $Height2 = $row4['pos_Height'];
                $Width2 = $row4['Width'];
                $datetime2 = date('Y-m-d H:i:s');
                $to_save_field2 = ",'".$LineText2."','".$WordText2."','".$Left2."','".$Top2."','".$Height2."','".$Width2;
                $result_to_database2 = $row4['data'];
                $filename2 =$row4['file_name'];
                $target_file2 =$row4['id_save'];
                if($WordText2 !="" && $WordText2 !=" "){
                    $query_insert3 = "INSERT INTO `ocr_cabatho_data_sync_ok` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file2."','".str_replace("'","",$result_to_database2)."','".$filename2."','".$datetime2."'".$to_save_field2."','".$MaxHeight2."','".$MinTop2."')";
                    if($connection->query($query_insert3)){
                        echo 'ok moteur 2 inactive';
                    }else{
                        echo 'erreur moteur 2 inactive';
                    }
                }
            }
        }
    }
    function insert4($filename)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();


        $file_name = $filename;
        $array_check4 = [];
        $query3 = "SELECT * FROM ocr_cabatho_data_sync_ok WHERE file_name = '".$file_name."'";
        foreach($connection->fetchAll($query3) as $row3) {
            array_push($array_check4,$row3['WordText']);
        }
        $query5 = "SELECT * FROM ocr_cabatho_data_engine_active WHERE file_name = '".$file_name."'";
        foreach($connection->fetchAll($query5) as $row4) {
            if (in_array($row4['WordText'],$array_check4)){
                echo 'dededededede';
            }else{
                $MaxHeight2 = $row4['MaxHeight'];
                $MinTop2 = $row4['MinTop'];
                $LineText2 = $row4['LineText'];
                $WordText2 =preg_replace('/-/', '',$row4['WordText']);
                $Left2 = $row4['pos_Left'];
                $Top2= $row4['pos_Top'];
                $Height2 = $row4['pos_Height'];
                $Width2 = $row4['Width'];
                $datetime2 = date('Y-m-d H:i:s');
                $to_save_field2 = ",'".$LineText2."','".$WordText2."','".$Left2."','".$Top2."','".$Height2."','".$Width2;
                $result_to_database2 = $row4['data'];
                $filename2 =$row4['file_name'];
                $target_file2 =$row4['id_save'];
                if($WordText2 !="" && $WordText2 !=" "){
                    $query_insert4 = "INSERT INTO `ocr_cabatho_data_sync_ok` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file2."','".str_replace("'","",$result_to_database2)."','".$filename2."','".$datetime2."'".$to_save_field2."','".$MaxHeight2."','".$MinTop2."')";
                    if($connection->query($query_insert4)){
                        echo 'ok moteur 2 active';
                    }else{
                        echo 'erreur moteur 2 active';
                    }
                }
            }
        }
    }
}
