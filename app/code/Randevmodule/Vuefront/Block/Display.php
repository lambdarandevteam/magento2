<?php
namespace Randevmodule\Vuefront\Block;

class Display extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context
    )
    {
        parent::__construct($context);
    }

    public function sayHello()
    {
        if(isset($_GET['test'])){
            return __($_GET['test']);
        }else{
            return __('Hello World');
        }

    }
}