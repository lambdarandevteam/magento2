<?php

namespace Randevmodule\Catalog\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Randevmodule\Catalog\Model\Product\Type\ProductRepair;
use Magento\Catalog\Model\Product;

class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $_eavSetupFactory;
    
    /**
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->_eavSetupFactory = $eavSetupFactory;
    }
    
    /**
     * Install data
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // get an instance of eavSetup
        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
        
        // array with attributes we want to add to our product type
        $attributes = [
            'cost',
            'price',
            'special_price',
            'special_from_date',
            'special_to_date',
            'weight',
            'tax_class_id'
                  ];
        
        foreach ($attributes as $attribute) {
            // get array with product types applied to this attribute
            $applyToProductTypes = explode(',', $eavSetup->getAttribute(Product::ENTITY, $attribute, 'apply_to'));
            // add new product type to product type list
            $applyToProductTypes[] = ProductRepair::TYPE_CODE;
            // apply to attribute
            $eavSetup->updateAttribute(Product::ENTITY, $attribute, 'apply_to', implode(',', $applyToProductTypes));
        }
    }
}